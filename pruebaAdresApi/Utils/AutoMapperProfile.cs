﻿using AutoMapper;
using pruebaAdresApi.DTO;
using pruebaAdresApi.Models;
using System.Globalization;

namespace pruebaAdresApi.Utils
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Adquisicion, AdquisicionDTO>()
                .ForMember(desp => desp.FechaAdquisicion, opt => opt.MapFrom(orig => orig.FechaAdquisicion.ToString("yyyy/MM/dd")));
            CreateMap<AdquisicionDTO, Adquisicion>()
                .ForMember(desp => desp.FechaAdquisicion, opt => opt.MapFrom(orig => DateTime.ParseExact(orig.FechaAdquisicion, "dd/MM/yyyy", CultureInfo.InvariantCulture) ));


            CreateMap<Historial, HistorialDTO>().ReverseMap();

        }
    }
}
