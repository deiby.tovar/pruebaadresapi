﻿namespace pruebaAdresApi.DTO
{
    public class HistorialDTO
    {
        public int Id { get; set; }

        public DateTime CreateAt { get; set; } = DateTime.Now;

        public string Action { get; set; } = string.Empty;

        public string? OldValues { get; set; } = string.Empty;

        public string NewValues { get; set; } = string.Empty;
    }
}
