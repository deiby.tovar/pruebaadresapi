﻿namespace pruebaAdresApi.DTO
{
    public class AdquisicionDTO
    {
        public int Id { get; set; }
        public decimal Presupuesto { get; set; }
        public string Unidad { get; set; } = string.Empty;
        public string Tipo { get; set; } = string.Empty;
        public int Catidad { get; set; }
        public decimal ValorUnit { get; set; }
        public decimal ValorTotal { get; set; }
        public string FechaAdquisicion { get; set; } = string.Empty;

        public string Proveedor { get; set; } = string.Empty;
        public string Documentacion { get; set; } = string.Empty;
    }
}
