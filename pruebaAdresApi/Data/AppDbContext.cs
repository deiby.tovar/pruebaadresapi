﻿using Microsoft.EntityFrameworkCore;
using pruebaAdresApi.Models;

namespace pruebaAdresApi.Data
{
    public class AppDbContext: DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }


        public DbSet<Historial> Historials { get; set; }
        public DbSet<Adquisicion> Adquisicions { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Configuración de la relación uno a muchos entre Padre e Hijo
            modelBuilder.Entity<Historial>()
                .HasOne(h => h.Adquisicion)
                .WithMany(p => p.Historials)
                .HasForeignKey(h => h.AdquisicionId);
        }


    }
}
