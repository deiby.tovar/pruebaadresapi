﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using pruebaAdresApi.Data;
using pruebaAdresApi.DTO;
using pruebaAdresApi.Models;
using System.Dynamic;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace pruebaAdresApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdquisicionController : ControllerBase
    {
        private readonly AppDbContext appDbContex;

        private  IMapper _mapper;

        public AdquisicionController(AppDbContext appDbContext, IMapper _mapper)
        {
            this.appDbContex = appDbContext;
            this._mapper = _mapper;
        }

        [HttpPost]
        public async Task<ActionResult<AdquisicionDTO>> AddAquisicion(AdquisicionDTO newAquisicionDTO)
        {
            try
            {
                if (newAquisicionDTO != null)
                {
                    newAquisicionDTO.ValorTotal = newAquisicionDTO.ValorUnit * newAquisicionDTO.ValorTotal;
                    var adquisicion = _mapper.Map<Adquisicion>(newAquisicionDTO);
                    appDbContex.Adquisicions.Add(adquisicion);
                    await appDbContex.SaveChangesAsync();
                    if (adquisicion.Id != null)
                    {
                           
                        this.CreteHistorial("", JsonSerializer.Serialize(adquisicion), "create", adquisicion );   
                        return Ok(_mapper.Map<AdquisicionDTO>(adquisicion));
                    }
                    else
                    {
                        return StatusCode(StatusCodes.Status500InternalServerError);
                    }
                }

                return BadRequest("No create");
            }
            catch(Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
           
        }

        [HttpGet]
        public async Task<ActionResult<List<AdquisicionDTO>>> GetAdquisiciones() {
            var adquisiciones = await appDbContex.Adquisicions.Where(ad => ad.DeleteAt == null).ToListAsync();
            var adquisicionesDTO = _mapper.Map<List<AdquisicionDTO>>(adquisiciones);
            return Ok(adquisicionesDTO);
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<AdquisicionDTO>> GetAdquisicion(int id)
        {
            var adquisicion = await appDbContex.Adquisicions.FirstOrDefaultAsync(ad => ad.Id == id);
            if (adquisicion != null)
            {
                var adquisicionDto = _mapper.Map<AdquisicionDTO>(adquisicion);
                return Ok(adquisicionDto);
            }
            return NotFound("No Found");
        }


        [HttpPut("{id:int}")]
        public async Task<ActionResult<Adquisicion>> UpdateAdquisicion(int id, AdquisicionDTO adquisicionUpdateDto)
        {
            try
            {
                var adquisicion = await appDbContex.Adquisicions.FirstOrDefaultAsync(ad => ad.Id == id);
                if (adquisicion != null && adquisicion.DeleteAt == null)
                {
                    dynamic oldValues = new ExpandoObject();
                    dynamic newValues = new ExpandoObject();
                    var adquisicionModel = _mapper.Map<Adquisicion>(adquisicionUpdateDto);

                    if (adquisicion.Documentacion != adquisicionModel.Documentacion)
                    {
                        oldValues.Documentacion = adquisicion.Documentacion;
                        newValues.Documentacion = adquisicionModel.Documentacion;
                        adquisicion.Documentacion = adquisicionModel.Documentacion;
                    }

                    if (adquisicion.FechaAdquisicion != adquisicionModel.FechaAdquisicion)
                    {
                        oldValues.FechaAdquisicion = adquisicion.FechaAdquisicion;
                        newValues.FechaAdquisicion = adquisicionModel.FechaAdquisicion;
                        adquisicion.FechaAdquisicion = adquisicionModel.FechaAdquisicion;
                    }

                    if (adquisicion.Unidad != adquisicionModel.Unidad)
                    {
                        oldValues.Unidad = adquisicion.Unidad;
                        newValues.Unidad = adquisicionModel.Unidad;
                        adquisicion.Unidad = adquisicionModel.Unidad;
                    }

                    if (adquisicion.Catidad != adquisicionModel.Catidad)
                    {
                        oldValues.Catidad = adquisicion.Catidad;
                        newValues.Catidad = adquisicionModel.Catidad;
                        adquisicion.Catidad = adquisicionModel.Catidad;
                    }

                    if (adquisicion.Presupuesto != adquisicionModel.Presupuesto)
                    {
                        oldValues.Presupuesto = adquisicion.Presupuesto;
                        newValues.Presupuesto = adquisicionModel.Presupuesto;
                        adquisicion.Presupuesto = adquisicionModel.Presupuesto;
                    }

                    if (adquisicion.Tipo != adquisicionModel.Tipo)
                    {
                        oldValues.Tipo = adquisicion.Tipo;
                        newValues.Tipo = adquisicionModel.Tipo;
                        adquisicion.Tipo = adquisicionModel.Tipo;
                    }

                    if (adquisicion.ValorUnit != adquisicionModel.ValorUnit)
                    {
                        oldValues.ValorUnit = adquisicion.ValorUnit;
                        newValues.ValorUnit = adquisicionModel.ValorUnit;
                        adquisicion.ValorUnit = adquisicionModel.ValorUnit;
                    }

                    if (adquisicion.Proveedor != adquisicionModel.Proveedor)
                    {
                        oldValues.Proveedor = adquisicion.Proveedor;
                        newValues.Proveedor = adquisicionModel.Proveedor;
                        adquisicion.Proveedor = adquisicionModel.Proveedor;
                    }


                    adquisicion.ValorTotal = adquisicionModel.ValorUnit * adquisicionModel.Catidad;
                    
                    this.CreteHistorial(JsonSerializer.Serialize(oldValues), JsonSerializer.Serialize(newValues), "updated", adquisicion);
                    await appDbContex.SaveChangesAsync();
                    return Ok(adquisicionModel);

                }
                return NotFound("No Found");
            }
            catch(Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
           
        }


        [HttpDelete("{id:int}")]
        public async Task<ActionResult<Adquisicion>> DeleteAdquisicion(int id)
        {
            var adquisicion = await appDbContex.Adquisicions.FirstOrDefaultAsync(ad => ad.Id == id);
            if (adquisicion != null)
            {
                adquisicion.DeleteAt = DateTime.Now;
                //appDbContex.Adquisicions.Remove(adquisicion);
                await appDbContex.SaveChangesAsync();
                this.CreteHistorial("", "", "delete", adquisicion);
                return Ok(true);
            }
            return NotFound("No Found");
        }

        [HttpGet("{id:int}/historial")]
        public async Task<ActionResult<List<HistorialDTO>>> GetHistorial(int id)
        {
            var adquisicion = await appDbContex.Adquisicions.Include(h => h.Historials).FirstOrDefaultAsync(ad => ad.Id == id);
            if (adquisicion != null)
            {
                var historial = _mapper.Map<List<HistorialDTO>>(adquisicion.Historials);
                return Ok(historial);
            }
            return NotFound("No Found");
        }


        private async void CreteHistorial(string oldValues, string newValues, string action, Adquisicion adquisicion)
        {
            var historial = new Historial();
            historial.Adquisicion = adquisicion;
            historial.NewValues = newValues;
            historial.OldValues = oldValues;
            historial.Action = action;
            historial.AdquisicionId = adquisicion.Id;
            appDbContex.Historials.Add(historial);
            await appDbContex.SaveChangesAsync();
        }

    }
}
