﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace pruebaAdresApi.Migrations
{
    /// <inheritdoc />
    public partial class Initials : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Adquisicions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Presupuesto = table.Column<int>(type: "INTEGER", nullable: false),
                    Unidad = table.Column<string>(type: "TEXT", nullable: false),
                    Tipo = table.Column<string>(type: "TEXT", nullable: false),
                    Catidad = table.Column<int>(type: "INTEGER", nullable: false),
                    ValorUnit = table.Column<int>(type: "INTEGER", nullable: false),
                    ValorTotal = table.Column<int>(type: "INTEGER", nullable: false),
                    FechaAdquisicion = table.Column<DateTime>(type: "TEXT", nullable: false),
                    Documentacion = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Adquisicions", x => x.Id);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Adquisicions");
        }
    }
}
