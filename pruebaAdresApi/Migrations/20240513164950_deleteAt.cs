﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace pruebaAdresApi.Migrations
{
    /// <inheritdoc />
    public partial class deleteAt : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DeleteAt",
                table: "Adquisicions",
                type: "TEXT",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Historials",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CreateAt = table.Column<DateTime>(type: "TEXT", nullable: false),
                    Action = table.Column<string>(type: "TEXT", nullable: false),
                    OldValues = table.Column<string>(type: "TEXT", nullable: true),
                    NewValues = table.Column<string>(type: "TEXT", nullable: false),
                    AdquisicionId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Historials", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Historials_Adquisicions_AdquisicionId",
                        column: x => x.AdquisicionId,
                        principalTable: "Adquisicions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Historials_AdquisicionId",
                table: "Historials",
                column: "AdquisicionId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Historials");

            migrationBuilder.DropColumn(
                name: "DeleteAt",
                table: "Adquisicions");
        }
    }
}
