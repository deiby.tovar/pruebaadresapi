﻿namespace pruebaAdresApi.Models
{
    public class Historial
    {

        public int Id { get; set; }

        public DateTime CreateAt { get; set; } = DateTime.Now;

        public string Action { get; set; } = string.Empty;

        public string? OldValues { get; set; } = string.Empty;

        public string NewValues { get; set; } = string.Empty;

        public int AdquisicionId { get; set; }
        public  Adquisicion? Adquisicion { get; set; }
    }
}
