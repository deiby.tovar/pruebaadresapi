﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace pruebaAdresApi.Models
{
    public class Adquisicion
    {
        [Required]
        public int Id { get; set; }

        [Required]
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Presupuesto { get; set; }
        [Required]
        public string Unidad { get; set; } = string.Empty;
        [Required]
        public string Tipo { get; set; } = string.Empty;
        [Required]
        public int Catidad { get; set; }

        [Required]
        [Column(TypeName = "decimal(18, 2)")]
        public decimal ValorUnit { get; set; }

        [Required]
        [Column(TypeName = "decimal(18, 2)")]
        public decimal ValorTotal { get; set; }
        [Required]
        public DateTime FechaAdquisicion { get; set; } = DateTime.Now;
        [Required]
        public string Proveedor { get; set; } = string.Empty;
        [Required]
        public string Documentacion { get; set; } = string.Empty;

        public ICollection<Historial> Historials { get; set; }


        public Nullable<DateTime> DeleteAt { get; set; }

    }
}
